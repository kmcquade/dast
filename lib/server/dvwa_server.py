from __future__ import annotations

import os
import time
from typing import Sequence

from .host import Host


class DVWAServer:

    def __init__(self, run, port=80):
        self.run = run
        self.port = port
        self.host = Host()

    def start(self) -> DVWAServer:
        this_dir = os.path.dirname(os.path.realpath(__file__))

        self.run('docker run --rm '
                 '--name dvwa '
                 f'-v {this_dir}/dvwa.sql:/resources/dvwa.sql '
                 f'-p {self.port}:80 '
                 '-d vulnerables/web-dvwa:1.9')

        time.sleep(4)
        self.run('docker exec dvwa bash -c "mysql --user root --password=vulnerables </resources/dvwa.sql"')

        print(f'Damn vulnerable webapp server started at http://{self.host.name()}:{self.port}/login.php')
        print('Username: admin, Password: password')

        return self

    def stop(self) -> DVWAServer:
        self.run('docker rm --force dvwa >/dev/null 2>&1 || true')
        return self

    def user_login_configuration(self) -> Sequence[str]:
        return ['DAST_SUBMIT_FIELD=Login',
                'DAST_PASSWORD_FIELD=password',
                'DAST_USERNAME_FIELD=username',
                'DAST_PASSWORD=password',
                'DAST_USERNAME=admin',
                f'DAST_AUTH_URL={self.target()}/login.php',
                f'DAST_AUTH_EXCLUDE_URLS={self.target()}/logout.php']

    def target(self) -> str:
        return f'http://{self.host.external_name()}:{self.port}'
