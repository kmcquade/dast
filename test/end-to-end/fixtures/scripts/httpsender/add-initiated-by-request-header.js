/**
 * Called before sending the request to the server.
 * See https://github.com/zaproxy/zaproxy/blob/7515fd13ab6efc8f8127d4e917728479dca3c63b/zap/src/main/java/org/zaproxy/zap/extension/script/ExtensionScript.java#L1764
 *
 * @param msg       the HTTP message being sent/received
 * @param initiator the initiator of the request, for possible values see see https://github.com/zaproxy/zaproxy/blob/161f3e9563d3eb5843f83aea91e59fc6ccb3e282/zap/src/main/java/org/parosproxy/paros/network/HttpSender.java#L129
 * @param helper    the helper class that allows to send other HTTP messages
 */
function sendingRequest(msg, initiator, helper) {
	msg.getRequestHeader().setHeader('x-initiated-by', 'GitLab DAST');
}

/**
 * Called after receiving a response from the server.
 * See https://github.com/zaproxy/zaproxy/blob/7515fd13ab6efc8f8127d4e917728479dca3c63b/zap/src/main/java/org/zaproxy/zap/extension/script/ExtensionScript.java#L1764
 *
 * @param msg       the HTTP message being sent/received
 * @param initiator the initiator of the request, for possible values see see https://github.com/zaproxy/zaproxy/blob/161f3e9563d3eb5843f83aea91e59fc6ccb3e282/zap/src/main/java/org/parosproxy/paros/network/HttpSender.java#L129
 * @param helper    the helper class that allows to send other HTTP messages
 */
function responseReceived(msg, initiator, helper) {
    // intentionally blank
}
