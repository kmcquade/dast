from os import path
from pathlib import Path
from tempfile import TemporaryDirectory
from unittest import TestCase

from src.zap_gateway.zap_database import ZAPDatabase


class TestZAPDatabase(TestCase):

    def setUp(self) -> None:
        self.hsqldb_jar = path.join(path.dirname(__file__), 'lib', 'hsqldb-2.5.0.jar')
        pass

    def test_can_retrieve_inserted_values(self):
        with TemporaryDirectory() as database_dir:
            Path(f'{database_dir}/dast.session.data').touch()
            database = ZAPDatabase(database_dir, self.hsqldb_jar)

            database.execute('create table customer(id integer not null, name varchar(50) not null, primary key (id))')
            database.execute("insert into customer values (1, 'Fred');")
            database.execute("insert into customer values (2, 'Justine');")
            results = database.select_many('select * from customer order by id asc')

            self.assertEqual(2, len(results))
            self.assertEqual(('1', 'Fred'), results[0])
            self.assertEqual(('2', 'Justine'), results[1])

    def test_can_read_zap_database(self):
        database_dir = path.join(path.dirname(__file__), 'fixture', 'dast_database')
        database = ZAPDatabase(database_dir, self.hsqldb_jar)

        results = database.select_many('select HISTORYID, METHOD, URI '
                                       'from HISTORY '
                                       'where HISTORYID in (1, 8, 12)')

        self.assertEqual(3, len(results))
        self.assertEqual(('1', 'GET', 'http://host.docker.internal:8010/'), results[0])
        self.assertEqual(('8', 'GET', 'http://host.docker.internal:8010/robots.txt'), results[1])
        self.assertEqual(('12', 'POST', 'http://host.docker.internal:8010/myform'), results[2])

    def test_should_throw_error_if_database_not_present(self):
        database = ZAPDatabase('not.a.directory', self.hsqldb_jar)

        with self.assertRaises(RuntimeError) as error:
            database.select_many('select * from INFORMATION_SCHEMA.TABLES')

        self.assertIn('Failed to find ZAP database', str(error.exception))
        self.assertIn('not.a.directory', str(error.exception))

    def test_database_can_be_closed_without_connecting(self):
        database = ZAPDatabase('a_directory', self.hsqldb_jar)
        database.close()

    def test_can_reconnect_to_a_closed_database(self):
        with TemporaryDirectory() as database_dir:
            Path(f'{database_dir}/dast.session.data').touch()
            database = ZAPDatabase(database_dir, self.hsqldb_jar)

            database.execute('create table numbers(id integer not null, primary key (id))')
            database.execute('insert into numbers values (789);')
            database.close()

            database = ZAPDatabase(database_dir, self.hsqldb_jar)
            results = database.select_many('select id from numbers')

            self.assertEqual(1, len(results))
            self.assertEqual('789', results[0][0])
