from unittest import TestCase

from src.models import Rules
from test.unit.factories.models import f_rule


class TestRules(TestCase):

    def test_is_subscriptable(self):
        rule = f_rule()
        rules = Rules([rule])

        self.assertEqual(rules[0], rule)

    def test_can_determine_length(self):
        rules = Rules([(f_rule())])
        self.assertEqual(1, len(rules))

    def test_should_return_disabled_rules(self):
        rules = Rules([f_rule(name='enabled', is_enabled=True),
                       f_rule(name='disabled', is_enabled=False)])

        disabled = rules.disabled()

        self.assertEqual(1, len(disabled))
        self.assertEqual('disabled', disabled[0].name())
