import unittest

from src.zap_gateway.http_headers_parser import HttpHeadersParser


class TestHttpHeadersParser(unittest.TestCase):

    def test_returns_no_headers_when_empty_input_provided(self):
        http_headers = HttpHeadersParser().parse('')
        self.assertEqual(len(http_headers), 0)

    def test_returns_no_headers_when_no_input_provided(self):
        http_headers = HttpHeadersParser().parse(None)
        self.assertEqual(len(http_headers), 0)

    def test_returns_no_headers_when_zero_headers_provided(self):
        http_headers = HttpHeadersParser().parse('GET http://host.docker.internal:8010/ HTTP/1.1')
        self.assertEqual(len(http_headers), 0)

    def test_parses_http_request_headers(self):
        raw_headers = '\r\n'.join(['GET http://host.docker.internal:8010/ HTTP/1.1',
                                   'User-Agent: python-requests/2.20.1',
                                   'Accept: */*',
                                   'Connection: keep-alive',
                                   'Host: host.docker.internal:8010',
                                   ])

        expected_request_headers = [
            {'name': 'Accept', 'value': '*/*'},
            {'name': 'Connection', 'value': 'keep-alive'},
            {'name': 'Host', 'value': 'host.docker.internal:8010'},
            {'name': 'User-Agent', 'value': 'python-requests/2.20.1'},
        ]

        http_headers = HttpHeadersParser().parse(raw_headers)
        self.assertEqual(expected_request_headers, http_headers.to_list_of_dicts())

    def test_parses_response_headers(self):
        raw_headers = '\r\n'.join(['HTTP/1.1 200 OK',
                                   'Server: nginx/1.17.6',
                                   'Date: Wed, 29 Jul 2020 05:17:10 GMT',
                                   'Content-Type: text/html',
                                   'Content-Length: 277',
                                   'Last-Modified: Wed, 29 Jul 2020 04:13:57 GMT',
                                   'Connection: keep-alive',
                                   'ETag: "5f20f785-115"',
                                   'Accept-Ranges: bytes',
                                   '',
                                   ])
        expected_response_headers = [
            {'name': 'Accept-Ranges', 'value': 'bytes'},
            {'name': 'Connection', 'value': 'keep-alive'},
            {'name': 'Content-Length', 'value': '277'},
            {'name': 'Content-Type', 'value': 'text/html'},
            {'name': 'Date', 'value': 'Wed, 29 Jul 2020 05:17:10 GMT'},
            {'name': 'ETag', 'value': '"5f20f785-115"'},
            {'name': 'Last-Modified', 'value': 'Wed, 29 Jul 2020 04:13:57 GMT'},
            {'name': 'Server', 'value': 'nginx/1.17.6'},
        ]

        http_headers = HttpHeadersParser().parse(raw_headers)
        self.assertEqual(expected_response_headers, http_headers.to_list_of_dicts())

    def test_parses_headers_with_colons(self):
        raw_headers = '\r\n'.join(['GET http://host.docker.internal:8010/ HTTP/1.1',
                                   "Content-Security-Policy-Report-Only: default-src https: data: wss: 'unsafe-eval' "
                                   "'unsafe-inline'; report-uri https://cdnjs.site.com/cdn-cgi/beacon/csp?"
                                   'req_id=5cfe08db9dafe0ce',
                                   ])

        expected_request_headers = [{
            'name': 'Content-Security-Policy-Report-Only',
            'value': "default-src https: data: wss: 'unsafe-eval' "
                     "'unsafe-inline'; report-uri https://cdnjs.site.com/cdn-cgi/beacon/csp?"
                     'req_id=5cfe08db9dafe0ce',
        }]

        http_headers = HttpHeadersParser().parse(raw_headers)
        self.assertEqual(expected_request_headers, http_headers.to_list_of_dicts())

    def test_can_parse_invalid_headers(self):
        raw_headers = '\r\n'.join(['GET http://host.docker.internal:8010/ HTTP/1.1',
                                   'header_name'])

        http_headers = HttpHeadersParser().parse(raw_headers)
        self.assertEqual([{'name': 'header_name', 'value': ''}], http_headers.to_list_of_dicts())
