from typing import List

from src.models import AggregatedAlert, Alert
from .f_alert import f_alert


def f_aggregated_alert(alerts: List[Alert] = [f_alert(), f_alert()]) -> AggregatedAlert:
    return AggregatedAlert(alerts)
