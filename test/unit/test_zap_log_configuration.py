import unittest
from io import StringIO

from src.zap_log_configuration import ZAPLogConfiguration
from test.unit.mock_config import ToConfig


class TestZAPLogConfiguration(unittest.TestCase):

    def setUp(self):
        self.stringio = StringIO()
        self.external_config = ToConfig(zap_log_configuration=None)
        self.extra_properties = 'HttpSender=DEBUG;CRAWLER=WARN'
        self.base_properties = (
            '# Properties below added programmtically by DAST\n'
            'log4j.rootLogger=INFO, R\n'
            'log4j.appender.R=org.apache.log4j.RollingFileAppender\n'
            'log4j.appender.R.File=${zap.user.log}/zap.log\n'
            'log4j.appender.R.MaxFileSize=1024MB\n'
            'log4j.appender.R.MaxBackupIndex=3\n'
            'log4j.appender.R.layout=org.apache.log4j.PatternLayout\n'
            'log4j.appender.R.layout.ConversionPattern=%d [%-5t] %-5p %c{1} - %m%n\n'
            'log4j.logger.org.apache.commons.httpclient=ERROR\n'
            'log4j.logger.net.htmlparser.jericho=OFF\n'
            'log4j.logger.com.crawljax.core.Crawler = WARN\n'
            'log4j.logger.com.crawljax.core.state.StateMachine = WARN\n'
            'log4j.logger.com.crawljax.core.UnfiredCandidateActions = WARN\n'
            'log4j.logger.hsqldb.db.HSQLDB379AF3DEBD.ENGINE = WARN'
        )

    def test_path(self):
        config = ZAPLogConfiguration(self.external_config)
        self.assertEqual(config.get_path(), '/app/zap/log4j.properties')

    def test_writes_base_properties(self):
        config = ZAPLogConfiguration(self.external_config)
        config.write_log_properties(self.stringio)
        self.assertEqual(self.stringio.getvalue(), self.base_properties)

    def test_appends_extra_properties(self):
        config = ZAPLogConfiguration(ToConfig(zap_log_configuration=self.extra_properties))
        config.write_log_properties(self.stringio)
        expectation = self.base_properties + '\n' + self.extra_properties.replace(';', '\n')
        self.assertEqual(self.stringio.getvalue(), expectation)
