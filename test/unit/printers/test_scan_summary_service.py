from unittest import TestCase
from unittest.mock import MagicMock, call

from src.services import ScanSummaryService
from test.unit.factories.models import f_alert, f_alerts, f_rule, f_rules
from test.unit.factories.models.http import http_message as f_http_message, \
                                            http_response as f_http_response


class TestScanSummaryService(TestCase):

    def test_print_all_executed_prints_each_rule(self):
        rules = f_rules(f_rule(name='Test Rule 1', rule_id='1'),
                        f_rule(name='Test Rule 2', rule_id='2'))
        system = MagicMock()

        ScanSummaryService(rules, f_alerts(None), system).print_results()

        system.notify.assert_has_calls([
            call('PASS: Test Rule 1 [1]'),
            call('PASS: Test Rule 2 [2]'),
        ])

    def test_prints_skipped_rules(self):
        rules = f_rules(f_rule(name='Skipped Rule', rule_id='10001', is_enabled=False))
        system = MagicMock()

        ScanSummaryService(rules, f_alerts(None), system).print_results()

        system.notify.assert_any_call('SKIP: Skipped Rule [10001]')

    def test_print_all_executed_adds_summary_of_urls_for_warn_rules(self):
        message = f_http_message(response=f_http_response(status=420))
        alrts = f_alerts(f_alert(plugin_id='1', url='https://democracynow.org', message=message))
        rules = f_rules(f_rule(name='Test Rule', rule_id='1'))
        system = MagicMock()

        ScanSummaryService(rules, alrts, system).print_results()

        system.notify.assert_has_calls([
            call('WARN: Test Rule [1] x 1'),
            call('\thttps://democracynow.org (420)'),
        ])

    def test_print_to_to_maximum_warning_urls(self):
        alrts = f_alerts(f_alert(plugin_id='1', url='https://page/a'),
                         f_alert(plugin_id='1', url='https://page/b'))
        rules = f_rules(f_rule(rule_id='1'))
        system = MagicMock()

        ScanSummaryService(rules, alrts, system, max_rules_to_print=1).print_results()

        notifications = list(map(lambda calls: calls[1][0].strip(), system.notify.mock_calls))
        self.assertIn('https://page/a (200)', notifications)
        self.assertNotIn('https://page/b (200)', notifications)

    def test_print_summary_prints_counts_for_each_rule_status(self):
        alrts = f_alerts(
            f_alert(plugin_id='666', url='hhttp.ttps://democracynow.org', message=f_http_message()),
        )
        rules = f_rules(f_rule(rule_id='666', is_enabled=True),
                        f_rule(rule_id='2', is_enabled=True),
                        f_rule(rule_id='3', is_enabled=False))
        system = MagicMock()
        ScanSummaryService(rules, alrts, system).print_results()

        system.notify.assert_any_call('SUMMARY - PASS: 1 | WARN: 1 | SKIP: 1')
