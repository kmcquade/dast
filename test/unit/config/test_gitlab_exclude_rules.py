from unittest import TestCase
from unittest.mock import mock_open, patch

import yaml

from src.config.gitlab_exclude_rules import EXCLUDE_RULES_YML, gitlab_exclude_rules


class TestGitlabExcludeRules(TestCase):

    def test_gitlab_exclude_rules(self):
        mock_exclude_rules = {'exclude_rules': [
            {'rule_id': 10000, 'name': 'Long Running Rule'},
            {'rule_id': 10001, 'name': 'False Positive Rule'},
        ]}

        with patch(
            'src.config.gitlab_exclude_rules.open',
            mock_open(read_data=yaml.dump(mock_exclude_rules)),
        ) as mock_open_yml:
            exclude_rules = gitlab_exclude_rules()

        mock_open_yml.assert_called_once_with(EXCLUDE_RULES_YML)
        self.assertEqual(exclude_rules, ['10000', '10001'])
