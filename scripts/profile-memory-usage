#!/usr/bin/env bash

PATH=/home/zap/.local/bin:$PATH
PLOT_TITLE="$DAST_PYTHON_MEMORY_PROFILE_REPORT"
PLOT_OUTPUT_FILE="/zap/wrk/profiling/$DAST_PYTHON_MEMORY_PROFILE_REPORT.png"
PROF_OUTPUT_FILE="/zap/wrk/profiling/$DAST_PYTHON_MEMORY_PROFILE_REPORT.dat"
METRICS_OUTPUT_FILE="/zap/wrk/profiling/$DAST_PYTHON_MEMORY_PROFILE_REPORT.txt"

mkdir -p /zap/wrk/profiling

mprof run --output "$PROF_OUTPUT_FILE" /app/analyze.py "$@"
EXIT_CODE=$?
mprof plot --flame --output "$PLOT_OUTPUT_FILE" --title "$PLOT_TITLE" "$PROF_OUTPUT_FILE"

# Sample PROF_OUTPUT_FILE format
#
# CMDLINE {cmd-string}
# MEM 1.171875 1594960379.3927
# MEM 16.234375 1594960379.4932
# MEM 25.488281 1594960379.5935
# MEM 28.968750 1594960379.6941
#
# Since the first line shows the cmd invocation we skip it using FNR == 1 {next}.

# Then we keep track of the min value of the second column before printing it.
# 5000 is used as an initial min value.
MIN=`awk 'FNR == 1 {next} BEGIN {min = 5000} {if ($2<min) min=$2} END {print min}' "$PROF_OUTPUT_FILE"`
echo "$DAST_PYTHON_MEMORY_PROFILE_REPORT"_min_mb "$MIN" > "$METRICS_OUTPUT_FILE"

# Then we keep track of the max value of the second column before printing it. 0
# is used an initial max value.
MAX=`awk 'FNR == 1 {next} BEGIN {max = 0} {if ($2>max) max=$2} END {print max}' "$PROF_OUTPUT_FILE"`
echo "$DAST_PYTHON_MEMORY_PROFILE_REPORT"_max_mb "$MAX" >> "$METRICS_OUTPUT_FILE"

# Then we the sum the second column before diving it by the total number of rows
# We subtract one 1 the first row doesn't contain a memory entry.
MEAN=`awk 'FNR == 1 {next} { total += $2 } END { print total/(NR - 1) }' "$PROF_OUTPUT_FILE"`
echo "$DAST_PYTHON_MEMORY_PROFILE_REPORT"_mean_mb "$MEAN" >> "$METRICS_OUTPUT_FILE"

exit $EXIT_CODE
