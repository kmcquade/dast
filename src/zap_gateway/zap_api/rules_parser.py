from typing import Any, List

from src.models import Rule, Rules


class RulesParser:

    def parse(self, results: List[Any]) -> Rules:
        rules = []

        for result in results:
            if type(result) != dict:
                continue

            rule = Rule(result.get('id'),
                        self.is_enabled(result),
                        result.get('name'))

            rules.append(rule)

        return Rules(rules)

    def is_enabled(self, result: Any) -> bool:
        return True if result.get('enabled') == 'true' else False
