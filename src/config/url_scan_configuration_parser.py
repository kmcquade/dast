import os
from typing import List

from src.config.argument_type import ArgumentType
from src.models import Target
from src.zap_gateway import Settings
from .invalid_configuration_error import InvalidConfigurationError


class URLScanConfigurationParser:

    DAST_PATHS_ENV_VAR = 'DAST_PATHS'
    DAST_PATHS_FILE_ENV_VAR = 'DAST_PATHS_FILE'

    def __init__(self, target: str, paths_to_scan_list: List[str], paths_to_scan_file_path: str, target_env_var: str):
        self._target = Target(target).reset_to_host()
        self._paths_to_scan_list = paths_to_scan_list
        self._paths_to_scan_file_path = paths_to_scan_file_path
        self._target_env_var = target_env_var

    def parse(self) -> List[str]:
        if self._paths_to_scan_list and not self._target:
            raise InvalidConfigurationError(f'When {self.DAST_PATHS_ENV_VAR} is defined {self._target_env_var} '
                                            'must also be set')

        if self._paths_to_scan_file_path and not self._target:
            raise InvalidConfigurationError(
                f'When {self.DAST_PATHS_FILE_ENV_VAR} is defined {self._target_env_var} must also be set')

        if self._paths_to_scan_list and self._paths_to_scan_file_path:
            raise InvalidConfigurationError(f'{self.DAST_PATHS_ENV_VAR} and {self.DAST_PATHS_FILE_ENV_VAR} '
                                            'can not be defined at the same time')

        if self._paths_to_scan_list:
            return self._build_urls(self._paths_to_scan_list)

        if self._paths_to_scan_file_path:
            full_path = self._paths_to_scan_file_full_path()

            with open(full_path, 'r') as filehandle:
                list_of_paths = filehandle.readlines()
                return self._build_urls(list_of_paths)

        return []

    def _paths_to_scan_file_full_path(self) -> str:
        wrk_dir_path = os.path.realpath(f'{Settings.WRK_DIR}{self._paths_to_scan_file_path}')

        if os.path.exists(wrk_dir_path) and wrk_dir_path.startswith(Settings.WRK_DIR):
            return wrk_dir_path

        ci_project_dir_path = os.path.realpath(f'{self._ci_project_dir}{self._paths_to_scan_file_path}')

        if os.path.exists(ci_project_dir_path) and ci_project_dir_path.startswith(self._ci_project_dir):
            return ci_project_dir_path

        raise InvalidConfigurationError(
            f'{self._paths_to_scan_file_path} could not be found in either '
            f'{self._ci_project_dir} or {Settings.WRK_DIR}',
        )

    def _build_urls(self, path_list: List[str]) -> List[str]:
        return [ArgumentType.url(self._target.build_url(path)) for path in path_list]

    @property
    def _ci_project_dir(self) -> str:
        try:
            return f"{os.environ['CI_PROJECT_DIR']}/"
        except KeyError:
            raise InvalidConfigurationError(
                f'CI_PROJECT_DIR must be available when using DAST_PATHS_FILE outside of {Settings.WRK_DIR}',
            )
