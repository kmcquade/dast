from pathlib import Path
from typing import Dict, List, Union

import yaml


EXCLUDE_RULES_YML = Path(__file__).parent / 'exclude_rules.yml'


def gitlab_exclude_rules() -> List[str]:
    with open(EXCLUDE_RULES_YML) as exclude_rules_file:
        exclude_rules: Dict[str, List[Dict[str, Union[str, int]]]] = yaml.safe_load(exclude_rules_file)

        return [str(rule['rule_id']) for rule in exclude_rules['exclude_rules']]
